package com.lockme.user;

public class UserCredential {
    
    private String siteName;
    private String socialMediaUsername;
    private String socialMediapassword;
    
    public UserCredential(){
                
    }
    
    public UserCredential(String siteName, String username, String password){
                this.siteName = siteName;
                this.socialMediaUsername = username;
                this.socialMediapassword= password;
    }

      
    public String getSitename(){
                return siteName;
    }
    public void setSitename(String siteName) {
    	 this.siteName=siteName;
    }
    
       
    public String getSocialMediaUsername(){
                return socialMediaUsername;
    }
    
    public void setSocialMediaUsername(String socialMediaUsername){
        this.socialMediaUsername = socialMediaUsername;
}
    
    public void setSocialMediaPassword(String password){
                this.socialMediapassword = password;
    }
    
    public String getSocialMediaPassword(){
                return socialMediapassword;
    }
    
    @Override
    public String toString(){
                
                return "For the SiteID: "+ siteName+" The username is: "+ socialMediaUsername + " and the password is: "+ socialMediapassword;
    }
}


